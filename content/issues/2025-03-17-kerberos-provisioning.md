---
title: Red Hat Ranch Outage - provisioning constrained
date: 2025-03-17 09:00:00
resolved: true
resolvedWhen: 2025-03-18 08:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 2025-03-18 08:00 UTC - The problem was addressed, make sure to restart your testing if you were affected by the outage. Sorry for the incovenience.

**Outage** 2025-03-17 09:00 UTC - Red Hat Ranch testing is disrupted due to provisioning being affected by inablity to talk to services protected by Kerberos. This severly reduces the number of workers taking care of provisioning, and disables Beaker-backed provisioning.
