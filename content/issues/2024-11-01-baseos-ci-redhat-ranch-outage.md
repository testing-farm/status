---
title: BaseOS CI and Red Hat Ranch affected by provisioning outages. BaseOS CI down.
date: 2024-11-01 00:00:00
resolved: true
resolvedWhen: 2024-11-01 09:00:00
severity: disrupted
affected:
  - BaseOS CI
  - Red Hat Ranch
section: issue
---

**Resolved** 2024-11-01 09:00 UTC - Outage was resolved. Please restart jobs if ended in error state.

**Outage** 2024-11-01 00:00 UTC - BaseOS Jenkins is down, and Red Hat ranch provisioning ending in error state.
