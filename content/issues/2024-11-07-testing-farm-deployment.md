---
title: Testing Farm 2024-10.1 released
date: 2024-11-07 08:00:00
resolved: true
resolvedWhen: 2024-11-11 08:00:00
severity: notice
affected:
  - Public Ranch 
  - Red Hat Ranch
section: issue
---

**Resolved** 2024-11-11 08:00 UTC - Everything looks ok, end of outage

**Update** 2024-11-11 09:00 UTC - Release should be stable, the other outages are not related to the release itself. Anyway, keeping this here as a note for one more week to raise awareness in case somebody runs into issues.

**Update** 2024-11-07 18:30 UTC - We should be stable now and deployed to 2024-10.1. There can be an issue where the results viewer is not loading some older results, no data gone, just it does nto present the data and you get an empty screen, We are trying to resolve it.

**Update** 2024-11-07 09:30 UTC - A bit of queue can happen. Please be patient.

**Update** 2024-11-07 09:00 UTC - Note that the new release is introducing some breaking changes, see a preview in the https://gitlab.com/testing-farm/docs/root/-/merge_requests/153[release notes draft].

**Update** 2024-11-07 08:10 UTC - A short outage caused some jobs to error out on Public Ranch, please restart your jobs.

**Info** 2024-11-07 08:00 UTC - We are deploying Testing Farm 2024-10.1. We are trying to avoid outages, but if you see something, check this status.
