---
title: Red Hat Ranch - deploying 2023-03.1 release - large queue expected
date: 2023-04-06 10:00:00
resolved: true
resolvedWhen: 2023-04-06 14:00:00
severity: notice
affected:
  - Red Hat Ranch
section: issue
---

**Notice** 2023-04-06 14:00 UTC - Testing Farm 2023-03.1 was deployed to Red Hat ranch \o/

**Notice** 2023-04-06 10:00 UTC - we deploying Testing Farm 2023-03.1 to Red Hat ranch, larger queue is expected.
