---
title: All Fedora images outdated, public ranch requests processing paused.
date: 2024-08-05 04:00:00
resolved: true
resolvedWhen: 2024-08-05 13:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2024-08-05 13:00 UTC - The outage was resolved by uploading images to Fedora AWS ourselves. We are still waiting for a proper fix on fedora infrasturcture side.

**Update** 2024-08-05 12:30 UTC - The upload is progressing, we had to implement few features to make the uploaded images look same as with the original ones, e.e. bios mode unset, gp3 used. ETA 1h.

**Update** 2024-08-05 09:30 UTC - The uploading of the images is broken, no new updates for long time :(. See https://pagure.io/fedora-infrastructure/issue/12110 for details. We stopped processing public ranch queue until the outage is resolved, we are uploading the images manually.

**Outage** 2024-08-05 08:00 UTC - All Fedora images have expired, we are investigating the issue.
