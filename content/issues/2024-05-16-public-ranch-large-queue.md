---
title: Public Ranch Outage - possible large queue due to deadlocked workers
date: 2024-05-15 20:00:00
resolved: true
resolvedWhen: 2024-05-16 19:30:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2024-05-16 19:30 UTC - problems should be gone

**Monitoring** 2024-05-16 07:30 UTC - the queue should be gone, we are investigating the root cause

**Outage** 2024-05-16 01:00 UTC - we started to investigate the issue

**Outage** 2024-05-15 20:00 UTC - large queue started to appear
