---
title: Expired Fedora-Rawhide images in Public Ranch
date: 2023-05-29 02:00:00
resolved: true
resolvedWhen: 2023-05-29 07:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** - Outage was resolved, failed jobs will require restart.

**Outage** - Images for Fedora Rawhide have expired. We are refreshing it. ETA 30 min.
