---
title: BaseOS CI limited due to outage due to  BRQ network maintanance.
date: 2023-11-12 00:00:00
resolved: true
resolvedWhen: 2023-11-13 12:00:00
severity: disrupted
affected:
  - BaseOS CI
section: issue
---

**Resolved** 12:00 UTC - Registry is back, outage is resolved. The tests should automatically resume, no manual intervention should be needed.

**Outage** 00:00 UTC - Our BRQ image registry went down after BRQ network maintanance. This is blocking BaseOS CI autoscaling, so the queue can get large. We are working to resolve the issue.
