---
title: BaseOS CI outage due to network issues
date: 2023-09-21 00:00:00
resolved: true
resolvedWhen: 2023-08-21 11:00:00
severity: disrupted
affected:
  - BaseOS CI
section: issue
---

**Resolved** 7:00 UTC - Problem was resolved by IT. BaseOS CI is back in action.

**Outage** 0:00 UTC - After restart DHCP does not resolve address of the machine. Working with IT to investigate the problem.
