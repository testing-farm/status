---
title: Testing Farm API outage and longer queues on both ranches.
date: 2024-08-08 04:00:00
resolved: true
resolvedWhen: 2024-08-08 06:15:00
severity: disrupted
affected:
  - Public Ranch
  - Red Hat Ranch
section: issue
---

**Outage** 2024-08-08 06:15 UTC - We resumed normal operation. Outage is over.

**Outage** 2024-08-08 04:00 UTC - We experienced an api outage. If you encounted some issues, please restart your requests.
