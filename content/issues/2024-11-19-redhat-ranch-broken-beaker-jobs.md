---
title: changed script for postinstall broke jobs in beaker
date: 2024-11-19 17:00:00
resolved: true
resolvedWhen: 2024-11-21 07:30:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 2024-11-21 07:30 UTC - revert deployed, restart affected jobs if needed

**Outage** 2024-11-21 07:00 UTC - recent change broke jobs running in beaker
