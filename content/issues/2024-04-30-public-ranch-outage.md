---
title: Public Ranch Outage - large queue, high error rate due to broken workers
date: 2024-04-30 18:00:00
resolved: true
resolvedWhen: 2024-04-30 21:30:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Update** 21:30 UTC - Outage was resolved.

**Update** 20:30 UTC - Issue was addressed. Queue is being processed. Some jobs might have failed, please restart your jobs if they errored out.

**Outage** 18:00 UTC - We broke our workers and they are not able to connect to our provisioner. We are investigating the issue.
