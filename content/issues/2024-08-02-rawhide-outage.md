---
title: Fedora Rawhide outdated image
date: 2024-08-02 08:00:00
resolved: true
resolvedWhen: 2024-08-02 09:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Update** 2024-08-02 09:00 UTC - New configuration deployed, fixing Rawhide provisioning in public ranch

**Outage** 2024-08-02 08:00 UTC - Rawhide image has been deleted and automation for updating looks broken, investigating.
