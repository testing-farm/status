---
title: CentOS Stream 9 repositories broken in CI
date: 2022-06-16 09:00:00
resolved: true
resolvedWhen: 2022-06-16 15:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Updated** 2022-06-16 15:00:00 UTC - Things look back to normal. Resolving the outage.

**Updated** 2022-06-16 10:00:00 UTC - The underlying infra problem had been fixed alreday by CentOS infra team, but it might take some time for mirrors to pick up the fixes. More info in https://pagure.io/centos-infra/issue/812. Seems the issues is affecting only Cs9, other supported composes should not be affected. We will be monitoring the problem and update the status once things settle.

**Outage** 2022-06-16 9:00:00 UTC - We are seeing a lot of errors from CentOS Stream 9 related to broken hashes in repositories. The root cause is currently unknown, reported to `centos-infra` in https://pagure.io/centos-infra/issue/812
