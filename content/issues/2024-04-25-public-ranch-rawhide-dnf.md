---
title: Public Ranch Outage - Broken Fedora Rawhide images
date: 2024-04-25 00:00:00
resolved: true
resolvedWhen: 2024-04-25 11:00:00
severity: disrupted
affected:
  - Public Ranch 
section: issue
---

**Resolved** 11:00 UTC Workaround with installing python3-dnf deployed

**Outage** 00:00 UTC - Our images in public ranch have broken dnf installation, missing python3-dnf
