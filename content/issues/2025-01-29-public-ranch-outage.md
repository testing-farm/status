---
title: Public Ranch Outage - artifacts storage out of space
date: 2025-01-29 16:00:00
resolved: true
resolvedWhen: 2025-01-29 18:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Outage** 18:00 UTC - Resolve outage, it was resolved in ~ 2 hours.

**Outage** 16:00 UTC - Artifacts storage on public storage went full. We are working on the issue.
