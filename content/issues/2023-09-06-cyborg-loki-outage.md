---
title: Cyborg Loki is being migrated to S3 storage and better deployment
date: 2023-09-06 08:00:00
resolved: true
resolvedWhen: 2023-12-31 12:00:00
severity: disrupted
affected:
  - Cyborg Loki
section: issue
---

**Resolved** - Resolved by loosing all data and redeployment.

**Outage** 8:00 UTC - Cyborg Loki is unavailable, it is being migrated to S3 storage and better deployment. ETA 24 - 48 hours
