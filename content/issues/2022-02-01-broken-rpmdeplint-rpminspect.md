---
title: Broken rpmdeplint and rpminspect in Fedora CI
date: 2022-01-31 22:00:00
resolved: true
resolvedWhen: 2022-02-02 14:30:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Update** - The underlying problem was fixed. Rpminspect and RPMdeplint works again.

**Update** - We are working on fixing the problem, it was broken with our migration from CentOS-8 to CentOS-Stream-8.

**Investigating** - We're aware of an issue affecting rpmdeplint and rpminspect on Public Ranch.
