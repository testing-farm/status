---
title: Red Hat Ranch & BaseOS CI - outage due to elevated cloud costs
date: 2023-05-02 08:00:00
resolved: true
resolvedWhen: 2023-04-28 12:00:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---

**Resolved** 2023-05-02 12:00 UTC - Problem was resolved. Queue was resolved, requests should start resuming, no issues are expected.

**Outage** 2023-05-02 08:00 UTC - We stopped BaseOS CI and Testing Farm Red Hat ranch, because we see Artemis provisioning c5.24xlarge machines, we are investigation why. This is causing elevated costs.
