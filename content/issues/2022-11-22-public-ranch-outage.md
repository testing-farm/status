---
title: Public Ranch Outage - artifacts storage out of inodes
date: 2022-11-22 14:00:00
resolved: true
resolvedWhen: 2022-11-22 20:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Follow-up** - It might be needed to restart your jobs, they might have failed without logs, as the artifacts were not correctly archived.

**Resolved** 20:00 UTC - The issue was identified and workarounded. We got out of inodes on the 10TB storage. Follow up in TFT-1735.

**Outage** 14:00 UTC - The artifacts.dev.testing-farm.io is out of inodes, we are looking into cleaning it up and understanding why it happened.
