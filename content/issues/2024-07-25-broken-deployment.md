---
title: Public and Redhat Ranch Outage - broken deployment
date: 2024-07-25 13:00:00
resolved: true
resolvedWhen: 2024-07-25 19:45:00
severity: disrupted
affected:
  - Public Ranch
  - Red Hat Ranch
section: issue
---

**Update** 19:45 UTC - almost all jobs from queue has been processed.

**Update** 18:18 UTC - deployemnt fixed, queues are being processed but it will take some time.

**Outage** 13:00 UTC - Issue with deployment affecting all new jobs.
