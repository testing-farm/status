---
title: Red Hat Ranch and BaseOS-CI Outage - broken krb infra
date: 2024-05-28 18:00:00
resolved: true
resolvedWhen: 2024-05-29 10:00:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---

**Resoled** 2024-05-29 10:00 UTC - Change related to krb infra reverted, everything works now.

**Update** 2024-05-29 9:00 UTC - BaseOS-CI partially unblocked by deploying workaround (missing fix in artemis)

**Update** 2024-05-29 10:00 UTC - something has changed in kerberos infra leading to broken keytab. All requests to beaker are affected
