---
title: CentOS Stream Koji Outage
date: 2022-08-11 08:00:00
resolved: true
resolvedWhen: 2022-08-11 10:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved**  The issue was identified and resolved by the CentOS Stream infrastructure team.

**Outage** 2022-08-11 08:00 UTC - The CentOS Stream Koji is down https://lists.centos.org/pipermail/centos-devel/2022-August/120524.html. No ETA available currently.
