---
title: Fedora Rawhide testing blocked on dnf5
date: 2023-06-21 12:30:00
resolved: true
resolvedWhen: 2023-06-21 17:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** - All issues were addressed, Fedora-Rawhide testing works again \o/

**Outage** - The switch to dnf5 broke Testing Farm. We are trying to hotpatch the problems to bring back testing against Rawhide.
