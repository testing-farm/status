---
title: Fedora CI installability test affected by systemd/selinux issue.
date: 2023-11-23 00:00:00
resolved: true
resolvedWhen: 2024-02-09 10:00:00
severity: disrupted
affected:
  - Fedora CI / installability
section: issue
---

**Resolved** 2024-02-09 10:00 UTC - The issue is resolved, please let us know if you would have still issues.

**Update** 2023-11-28 19:00 UTC - The problem still persists. Please follow this bugzzila https://bugzilla.redhat.com/show_bug.cgi?id=2250930 for more details.

**Outage** 2023-11-23 00:00 UTC - A systemd/selinux issue is causing unexpected failures in installability for a lot of components. Also some of the functional tests might be affected. For more info see https://pagure.io/fedora-ci/general/issue/447
