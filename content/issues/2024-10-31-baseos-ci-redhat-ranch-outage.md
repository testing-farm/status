---
title: BaseOS CI and Red Hat Ranch affected by OpenShift outage
date: 2024-10-31 12:00:00
resolved: true
resolvedWhen: 2024-11-01 00:00:00
severity: disrupted
affected:
  - BaseOS CI
  - Red Hat Ranch
section: issue
---

**Partial outage** 2024-10-31 12:00 UTC - BaseOS CI and Red Hat Ranch affected by maintenance of RDU2 OpenShift.
