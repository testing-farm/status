---
title: Red Hat Ranch and BaseOS-CI Outage - dns misconfiguration, some crucial sites not accessible
date: 2024-08-13 15:40:00
resolved: true
resolvedWhen: 2024-08-14 00:50:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---
**Resolved** 2024-08-14 00:50 UTC - both services fully operational.

**Update** 2024-08-13 23:42 UTC - dns issues looks resolved, baseosci resumed (long queque),  testing farm under testing

**Outage** 2024-08-13 15:50 UTC - because of issue with dns servers, some internal sites are not accessible, BaseOS-CI is suspended to lower impact
