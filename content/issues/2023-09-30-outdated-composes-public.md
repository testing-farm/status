---
title: Expired images on Public ranch.
date: 2023-09-30 04:00:00
resolved: true
resolvedWhen: 2023-10-02 04:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2023-10-02 04:00 UTC - Images were updated, problems should be gone. Restart of testing will be needed for affected users.

**Outage** 2023-09-30 4:00 UTC - Some of the images have expired on Public Ranch.
