---
title: Brew outage https://brewweb.engineering.redhat.com/brew/
date: 2024-12-10 09:00:00
resolved: true
resolvedWhen: 2024-12-19 13:45:00
severity: disrupted
affected:
  - BaseOS CI
  - Red Hat Ranch
  - Any testers using brew
section: issue
---

**Outage** 2024-12-19 11:30:00 UTC - Brew service is down. All testing relying on Brew are affected. See https://brewweb.engineering.redhat.com/brew/
