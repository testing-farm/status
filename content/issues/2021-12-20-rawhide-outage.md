---
title: Issues Testing Fedora Rawhide
date: 2021-12-19 20:00:00
resolved: true
resolvedWhen: 2021-12-20 12:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

*Update* - The incident has been resolved. Root cause of the problem is [Bug 2034336](https://bugzilla.redhat.com/show_bug.cgi?id=2034336). We rolled back to Fedora Rawhide image from `2021-12-12` which does not contain the issue.

*Investigating* - We're aware of an issue affecting all testing on Fedora Rawhide. It appeared as we upgraded to the latest image `2021-12-17`.
