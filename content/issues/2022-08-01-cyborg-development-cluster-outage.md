---
title: Cyborg Development Cluster outage
date: 2022-08-01 11:00:00
resolved: true
resolvedWhen: 2022-08-01 13:00:00
severity: disrupted
affected:
  - Cyborg Development Cluster
section: issue
---

**Update** - Cluster was reinstalled.

**Outage** - The Cyborg Development Cluster is currently down, seems due to PSI outage during weekend, we are reinstalling it.
