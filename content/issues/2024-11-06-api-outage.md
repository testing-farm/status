---
title: Testing Farm API down
date: 2024-11-06 14:30:00
resolved: true
resolvedWhen: 2024-11-06 15:15:00
severity: disrupted
affected:
  - Red Hat Ranch
  - Public Ranch
section: issue
---

**Resolved** 2024-11-06 15:10 UTC - The outage is over. API is back. Please restart your jobs if you were affected. We are sorry for the inconvenience.

**Outage** 2024-11-06 14:30 UTC - We are suffering from an API outage, jobs will break, can be stuck forever.
