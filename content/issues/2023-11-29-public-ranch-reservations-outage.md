---
title: Public Ranch reservations are broken
date: 2023-11-29 00:00:00
resolved: true
resolvedWhen: 2023-12-07 10:00:00
severity: notice
affected:
  - Public Ranch 
section: issue
---

**Resolved** 2023-12-07 10:00 UTC - Deployment has been changed to terragrunt, the issues is resolved, reservations should work according to the IP allowlist.

**Info** 2023-11-29 00:00 UTC - Due to deployment changes we broke the Testing Farm reservations in public, please bare with us until we resolve the issues. We hope to resolve the problems today.
