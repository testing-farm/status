---
title: Public Ranch Outage - artifacts storage out of space
date: 2024-05-09 16:00:00
resolved: true
resolvedWhen: 2024-05-09 19:30:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Update** 19:30 UTC - Old requests with large space usage were removed. Outage resolved.

**Outage** 16:00 UTC - Artifacts storage on public storage went full.
