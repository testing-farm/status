---
title: download.devel SSL certificate expired
date: 2024-11-24 21:00:00
resolved: true
resolvedWhen: 2024-11-25 10:30:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---

**Resolved** 2024-11-25 10:30 UTC - Outage was resolved by IT. Certificate was refreshed.

**Outage** 2024-11-24 23:00 UTC - See https://redhat.service-now.com/surl.do?n=INC3256074 for details.

**Outage** 2024-11-24 21:00 UTC - The download.devel SSL certificate has expired, this is affecting BaseOS CI and Testing Farm Red Hat ranch.
