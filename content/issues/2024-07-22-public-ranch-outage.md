---
title: Public Ranch Outage - complete outage, request will stay queued or error out
date: 2024-07-22 00:16:00
resolved: true
resolvedWhen: 2024-07-22 09:17:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 09:17 UTC - The issue was resolved, our postgres database went out of space in the early morning. All affected requests restarted. Fedora CI users of Testing Farm, will need to manually restart their testing. Packit users pipelines should automatically resolve.

**Outage** 07:17 UTC - Public Artemis stopped working, investigating. Until this is resolved, request will stay queued and will be processed later.
