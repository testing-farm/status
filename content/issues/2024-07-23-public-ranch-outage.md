---
title: Public Ranch Outage - artifacts out of space
date: 2024-07-23 06:00:00
resolved: true
resolvedWhen: 2024-07-23 10:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 10:00 UTC - The issue was resolved. Resolving outage.

**Update** 09:00 UTC - We started to investigate the issue and working on getting back TF public ranch into a good state.

**Outage** 06:00 UTC - Testing Farm Artifacts are out of space. 
