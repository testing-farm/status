---
title: Public Ranch outage - requests stuck in queued
date: 2023-01-24 15:00:00
resolved: true
resolvedWhen: 2023-01-25 10:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2023-01-25 10:00:00 UTC - Issue was resolved. All affected users should retry testing against Public ranch.

**Outage** 2023-01-24 15:00:00 UTC - Due to a deployment change all requests stuck in queued state.
