---
title: Fedora Rawhide outdated image
date: 2025-01-14 08:00:00
resolved: true
resolvedWhen: 2025-01-15 10:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2025-01-15 10:00 UTC - New configuration deployed, fixing Rawhide provisioning in public ranch.

**Outage** 2025-01-14 08:00 UTC - Rawhide image has been deleted and automation for updating looks broken, investigating.
