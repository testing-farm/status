---
title: Public Ranch Outage - bug in security groups cleanup.
date: 2025-03-06 15:30:00
resolved: false
#resolvedWhen: 2025-03-06 15:30:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Outage** 2025-03-06 15:30 UTC - Public Ranch provisioning is disrupted due to hitting the security groups limits.

**Update** 2025-03-06 22:00 UTC - Provisioning unblocked by deleting some security groups, we will monitor this.
