---
title: RHEL-9.6.0-image-mode is broken
date: 2024-11-26 00:00:00
resolved: true
resolvedWhen: 2024-12-05 15:30:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 2024-12-05 15:30 UTC - RHEL-9.6.0-image mode is available on Testing Farm and tmt again. Virtualization support and aarch64 is still in works, but outside of that it should be ready for consumption.

**Outage** 2024-11-26 00:00 UTC - The image mode MVP testing on RHEL-9.6.0 is currently blocked, the image has expired, we are working this out.
