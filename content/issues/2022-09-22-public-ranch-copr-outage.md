---
title: Public Ranch outage - Copr API v2 is deprecated
date: 2022-09-22 6:00:00
resolved: true
resolvedWhen: 2022-09-23 00:30:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** - Issue was hotfixed on all ranches via https://gitlab.com/testing-farm/gluetool-modules/-/merge_requests/223.

**Update** 13:00 - To mitigate consequences of the outage, whole Public Ranch was suspended.

**Outage** 08:00 - Copr deprecated API v2 today. We are working on migration to API v3. This affects all requests which test copr builds. All requests from Packit are affected.
