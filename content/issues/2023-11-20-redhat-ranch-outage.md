---
title: Testing Farm Red Hat Ranch outage during the weekend
date: 2023-11-19 06:00:00
resolved: true
resolvedWhen: 2023-11-20 09:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 2023-11-20 09:00 UTC - We do not know the root cause, but we will try to improve the liveness probes to autoheal the deployment if the problem happens. Tracked under https://issues.redhat.com/browse/TFT-2353[TFT-2353].

**Update** 2023-11-20 06:00 UTC - The provisioner is working again, we are trying to find out what happened and how to deal with the problem automatically.

**Outage** 2023-11-19 06:00 UTC - Our provisioner has stopped processing new requests during the weekend. We are investigating the issue.
