---
title: Public ranch outage - workers not able to connect to Artemis.
date: 2024-08-28 10:29:00
resolved: true
resolvedWhen: 2024-08-28 14:21:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---
**Resolved** 2024-08-28 14:21 UTC - the service is fully operational. If your requests are still failing, please restart them.

**Outage** 2024-08-28 10:29 UTC - because of misconfigured network in production environment, workers are not able to connect to Artemis.
