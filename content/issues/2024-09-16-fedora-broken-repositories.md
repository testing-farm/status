---
title: Public Ranch - broken Fedora repositories
date: 2024-09-16 00:00:00
resolved: true
resolvedWhen: 2024-09-16 19:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2024-09-16 19:00 UTC - The outage was resolved by the Fedora infrastructure team.

**Outage** 2024-09-16 00:00 UTC - We are experiencing outages due to broken repositories in Fedora. The issue is tracked by Fedora Infrastructure in this issue - https://pagure.io/fedora-infrastructure/issue/12183. The issue causes following failures in Testing Farm:

```
Updating and loading repositories:                                                            
 Fedora 41 openh264 (From Cisco) - x86_64             100% |   3.0 KiB/s |   6.0 KiB |  00m02s
 Fedora 41 - x86_64              100% [==================] |  12.3 KiB/s | 385.2 KiB | -00m00s
 Fedora 41 - x86_64              100% [==================] |  12.3 KiB/s | 385.2 KiB | -00m00s
 Fedora 41 - x86_64              100% [==================] |  12.3 KiB/s | 385.2 KiB | -00m00s
 Fedora 41 - x86_64              100% [==================] |  12.3 KiB/s | 391.1 KiB | -00m00s
>>> Downloading successful, but checksum doesn't match. Calculated: 48ea7729665cea44a1e7755055
 Fedora 41 - x86_64              100% [==================] |  12.3 KiB/s | 391.1 KiB | -00m00s
...snip...
>>> Downloading successful, but checksum doesn't match. Calculated: f6dc87bb2fab2e324c8c8f3980
>>> Downloading successful, but checksum doesn't match. Calculated: f6dc87bb2fab2e324c8c8f3980
>>> Downloading successful, but checksum doesn't match. Calculated: 48ea7729665cea44a1e7755055
>>> Downloading successful, but checksum doesn't match. Calculated: 48ea7729665cea44a1e7755055
>>> Downloading successful, but checksum doesn't match. Calculated: 48ea7729665cea44a1e7755055
>>> Downloading successful, but checksum doesn't match. Calculated: 48ea7729665cea44a1e7755055
>>> Downloading successful, but checksum doesn't match. Calculated: 48ea7729665cea44a1e7755055
>>> Downloading successful, but checksum doesn't match. Calculated: 48ea7729665cea44a1e7755055
>>> Downloading successful, but checksum doesn't match. Calculated: 48ea7729665cea44a1e7755055
>>> Downloading successful, but checksum doesn't match. Calculated: 48ea7729665cea44a1e7755055
>>> Downloading successful, but checksum doesn't match. Calculated: 48ea7729665cea44a1e7755055
>>> Downloading successful, but checksum doesn't match. Calculated: 48ea7729665cea44a1e7755055
>>> Librepo error: Cannot download repomd.xml: Cannot download repodata/repomd.xml: All mirror
Failed to download metadata (metalink: "https://mirrors.fedoraproject.org/metalink?repo=fedora-41&arch=x86_64") for repository "fedora"
 Librepo error: Cannot download repomd.xml: Cannot download repodata/repomd.xml: All mirrors were tried
```
