---
title: Public Ranch Outage - F40 images expired
date: 2024-05-06 0:00:00
resolved: true
resolvedWhen: 2024-05-06 07:30:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Update** 7:30 UTC - Issue was addressed. F40 image was updated.

**Outage** 00:00 UTC - F40 image got expired.
