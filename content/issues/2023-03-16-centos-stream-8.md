---
title: CentOS Stream 8 broken in Public Ranch
date: 2023-03-16 08:00:00
resolved: true
resolvedWhen: 2023-03-20 11:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2023-03-20 11:00 UTC - The problem was resolved. CentOS Stream 8 changed the naming in AWS, so we were using old image. Seems affected tests are now passing \o/.


**Outage** 2023-03-16 08:00 UTC - We are seeing various issues with the updated CentOS Stream 8 in CI. This is due to the update to RHEL-8.7.0 bits we believe. More information will land once we analyze the problems. The requests uusually error out unexpectedly.

Some example issues:

https://artifacts.dev.testing-farm.io/d5256819-3f06-4dc4-bf01-923c135de16a/
