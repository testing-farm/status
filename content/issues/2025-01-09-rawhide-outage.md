---
title: Fedora Rawhide outdated image
date: 2025-01-09 08:00:00
resolved: true
resolvedWhen: 2025-01-09 12:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Outage** 2025-01-09 12:00 UTC - Image updated, outage is resolved.

**Outage** 2025-01-09 08:00 UTC - Rawhide image has been deleted and new one updated yesterday is alredy missing too, investigating.
