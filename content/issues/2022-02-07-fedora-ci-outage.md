---
title: Fedora CI outage - AWS images are gone
date: 2022-02-05 20:00:00
resolved: true
resolvedWhen: 2022-02-07 21:25:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Update** - We uploaded our own images to overcome the problems with Fedora Infrastructure. The outage is resolved. Please restart jour tests jobs if you need them.

**Update** - Because still no ETA on fixing Fedora CI provided AWS images, we are trying to upload them to AWS ourselves.

**Update** - We found out all images are gone after the weekend from Fedora AWS - https://pagure.io/fedora-infrastructure/issue/10532. We are working with Fedora admins to get things in good shape, but there is currently no ETA as we might need to wait for US folks to come to work.
