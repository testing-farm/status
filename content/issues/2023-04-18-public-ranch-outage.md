---
title: Public Ranch - unexpected outage
date: 2023-04-18 14:00:00
resolved: true
resolvedWhen: 2023-04-18 20:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2023-04-18 20:00 UTC - Outage was resolved, it can take some time to process all requests.

**Ongoing** 2023-04-18 19:00 UTC - Unfortunately, it is taken even longer as expected. ETA hopefully today.

**Ongoing** 2023-04-18 19:00 UTC - Unfortunately, it is taken even longer as expected. ETA hopefully today.

**Ongoing** 2023-04-18 16:00 UTC - Unfortunately, it is taken longer as expected. ETA another 2 hours.

**Outage** 2023-04-18 14:00 UTC - There is an outage in public ranch, we need to redeploy. ETA 2 hours.
