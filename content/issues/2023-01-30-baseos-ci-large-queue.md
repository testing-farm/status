---
title: BaseOS CI - large queue, expect delays.
date: 2023-01-30 16:00:00
resolved: true
resolvedWhen: 2023-02-13 18:00:00
severity: notice
affected:
  - BaseOS CI
section: issue
---

**📓 Notice** 2023-02-13 18:00:00 UTC - We believe things are now a lot better, we are able to keep with the queue. Resolving.

**📓 Notice** 2023-01-30 16:00:00 UTC - We are experiencing a larger queue on BaseOS CI. This can affect `baseos-ci.*` and `leapp.*` tests. Delays are expected. We are having trouble scaling the deployment due to some bugs in the deployment :(
