---
title: Red Hat Ranch outage during weekend
date: 2023-09-17 08:00:00
resolved: true
resolvedWhen: 2023-09-17 16:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Outage** 16:00 UTC - Resolved.

**Outage** 8:00 UTC - We had an outage during the weekend in our provisioner. Some jobs might have failed. Please restart your testing if needed.
