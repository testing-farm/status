---
title: Red Hat Ranch - provisioning outage
date: 2023-04-03 08:00:00
resolved: true
resolvedWhen: 2023-03-29 12:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Outage** 2023-04-03 12:00 UTC - we believe the issue was resolved, please restart your jobs if needed.

**Outage** 2023-04-03 08:00 UTC - we are experiencing outage of our provisioner. It is expected some requests will end in error. Also we have a larger queue, so longer test times expected.
