---
title: BaseOS CI suspended for changes in UMB certificate.
date: 2024-10-15 17:00:00
resolved: true
resolvedWhen: 2024-10-15 23:00:00
severity: disrupted
affected:
  - BaseOS CI
section: issue
---

**Resolved 2024-10-15 23:55 UTC - queue resumed, still running jobs canceled & restarted

**Outage** 2024-10-15 17:00 UTC - queue suspended, except for dispatchers

**Planned maintenance** 2024-10-15 14:00 UTC - Planned maintenance of jenkins controllers. Impact: suspended queue since 17:00 UTC, expected end: 20:00 UTC
