---
title: Public Ranch large queue - expect delays
date: 2024-02-15 17:00:00
resolved: true
resolvedWhen: 2024-02-05 19:30:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 19:30 UTC - Queue is almost gone, resolving.

**Outage** 17:00 UTC - Testing Farm public ranch has long queue due to deployment. It might take a bit of time the queue to go down. Please be patient.
