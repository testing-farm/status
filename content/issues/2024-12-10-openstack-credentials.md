---
title: Testing Farm fails to authenticate to OpenStack
date: 2024-12-10 8:00:00
resolved: true
resolvedWhen: 2024-12-10 15:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Outage** 2024-12-10 15:00:00 UTC - Resolved by using new credentials for Openstack.

**Outage** 2024-12-10 08:00:00 UTC - Testing Farm fails to authenticate to OpenStack, we are investigating the root cause.
