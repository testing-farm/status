---
title: rpminspect fails with "internal libpod error"
date: 2022-02-08 12:00:00
resolved: true
resolvedWhen: 2022-04-09 13:00:00
severity: disrupted
affected:
  - Public Ranch
  - Red Hat Ranch
section: issue
---

**Resolved 2022-04-09** - Issue was resolved by updating to a podman pre-release which is fixing the bug.

**Update 2022-03-25** - Upstream has a fix for the problem. We are waiting for a custom module build with the fix, and we will update our workers with it.

**Update 2022-02-25** - We have found a minimal reproducer - https://github.com/containers/podman/issues/13227#issuecomment-1050997458

**Update 2022-02-25** - Investigation is still ongoing, we are not able to reproduce the problem manually. We will try today to update to podman 4.0.1 to see if it helps with the issue.

**Update 2022-02-14** - Issue was filled to podman - https://github.com/containers/podman/issues/13227

**Investigating** - We're aware of an issue affecting rpminspect and causing some of the tests blow up on errors like "Error: timed out waiting for file /var/lib/containers/storage/overlay-containers/4dbddac31d1a5529d055922e3fca8d200585b5ca562e7eb9976f32d7dd96b8f2/userdata/1e05be2210ea6433b84bba70c720b55586d4987fde7005428492e583d8ebc3a7/exit/4dbddac31d1a5529d055922e3fca8d200585b5ca562e7eb9976f32d7dd96b8f2: internal libpod error"
