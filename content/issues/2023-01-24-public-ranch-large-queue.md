---
title: Public Ranch - large queue, expect delays.
date: 2023-01-25 10:00:00
resolved: true
resolvedWhen: 2023-01-25 17:00:00
severity: notice
affected:
  - Public Ranch
section: issue
---

**Resolved** 2023-01-25 17:00:00 UTC - Whole queue was processed. No more delays are expected.

**Notice** 2023-01-25 15:00:00 UTC - We are about 60% done, workers scaled, we expect 1-2 hours to process all the queues.

**Notice** 2023-01-25 10:00:00 UTC - We are rerunning 1.2k jobs due to the outage. It might take some time to process.
