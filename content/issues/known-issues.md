---
title: Known issues
date: 2022-01-03 00:17:00
pin: false
informational: true
section: issue
---

* There are currently no known issues, if you have troubles, contact us :)

**Recently resolved**

* Rawhide image update is blocked on [BZ#2034336](https://bugzilla.redhat.com/show_bug.cgi?id=2034336) - resolved on `2021-01-03 00:12:00` - seems underlying problems have been resolved finally
