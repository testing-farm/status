---
title: Unexpected outage due to new release 2023-04.1
date: 2023-05-05 02:00:00
resolved: true
resolvedWhen: 2023-05-05 12:00:00
severity: disrupted
affected:
  - Public Ranch
  - Red Hat Ranch
section: issue
---

**Outage** 2023-05-05 12:00 UTC - Outage was resolved.

**Outage** 2023-05-05 02:00 UTC - We are deploying new Testing Farm, and it is not going super well. Expect delays and in case of unexpected errors, please restart your jobs.
