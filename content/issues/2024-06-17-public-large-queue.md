---
title: Public Ranch - Large queue
date: 2024-06-17 13:00:00
resolved: true
resolvedWhen: 2024-07-02 10:00:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---

**Warning** 2024-06-17 13:00 UTC - Public ranch has 1k+ queued jobs, seems Fedora CI load, be patient, it is getting progressed, but expect delays.
