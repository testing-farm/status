---
title: Public Ranch Outage - Expired images
date: 2024-04-15 00:00:00
resolved: true
resolvedWhen: 2024-04-15 08:30:00
severity: disrupted
affected:
  - Public Ranch 
section: issue
---

**Resolved** 08:30 UTC - Resolved. Images have been updated.

**Outage** 00:00 UTC - Our images in public ranch have expired. We are updating them.
