---
title: Public Ranch Outage - expired composes
date: 2025-02-02 04:00:00
resolved: true
resolvedWhen: 2025-02-03 08:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2025-02-03 08:00 UTC - Images had been refreshed. Please restart your testing, all jobs affected will error out eventually.

**Outage** 2025-02-02 04:00 UTC - Public ranch composes have expired, we are working on refreshing them. Sorry for the inconvenience.
