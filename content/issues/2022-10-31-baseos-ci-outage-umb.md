---
title: BaseOS CI Outage - UMB message sending
date: 2022-10-31 8:00:00
resolved: true
resolvedWhen: 2022-10-31 12:00:00
everity: disrupted
affected:
  - BaseOS CI 
section: issue
---

**Resolved** 12:00 UTC - The issue was resolved, the UMB cert was rotated. We are restarting all affected jobs. Processing the queue can take some time. Contact us in case of trouble.

**In Progress** 10:30 UTC - The issue was identified - an expired UMB certificate. We are working with IT to be able to rotate it. Current ETA is at least 2 more hours.

**Outage** 8:00 UTC - BaseOS CI is not processing any jobs currently, it is failing to send out any UMB message. We are investigating the problem.
