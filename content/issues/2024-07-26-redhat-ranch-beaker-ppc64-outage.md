---
title: Beaker PPC64 Outage - large queue because of limited hardware
date: 2024-07-26 00:00:00
resolved: true
resolvedWhen: 2024-08-02 00:00:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---

**Resolved** 2024-08-02 00:00 UTC - ppc64le queue times stayed below 4 hours for 3 days.

**Outage** 2024-07-26 00:00 UTC - due to a deteriorating PPC64 pools in Beaker, testing on this ARCH is not advised. Expect over 24 hours delay.
