---
title: Fedora Rawhide testing blocked on dnf5
date: 2023-06-22 09:00:00
resolved: true
resolvedWhen: 2023-07-14 9:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** - All issues were addressed, Fedora-Rawhide testing works again \o/

**Outage** - The switch to dnf5 broke Testing Farm again. Some use cases might work though, no current ETA to resolve all the problems. We will be posting updates here.
