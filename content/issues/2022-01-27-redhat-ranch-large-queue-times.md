---
title: Large queue times on Red Hat Ranch
date: 2022-01-27 10:30:00
resolved: true
resolvedWhen: 2022-02-25 12:52:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Update 2022-02-25** - Testing Farm was scaled and should have enough capacity for the current workload.

**Update 2022-02-01** - Currently the load is ok, but we are still working on a more permanent solution

**Update** - We are working on a fix to scale the ranch to handle the load.

**Investigating** - We're aware of an issue affecting queue times on Red Hat Ranch of Testing Farm. Testing can be delayed several hours. It is mostly affecting rpmdeplint and rpminspect.
