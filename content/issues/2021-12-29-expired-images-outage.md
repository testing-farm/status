---
title: Expired RHEL-8.6.0 and RHEL-9.0.0 images in Red Hat Ranch and BaseOS CI
date: 2021-12-29 11:00:00
resolved: true
resolvedWhen: 2022-01-03 00:12:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---

**Update** - Images were updated, failed tests restarted. In case you encounter issues, please restart your failed jobs.

**Update** - [Red Hat tracking issue](https://issues.redhat.com/browse/TFT-924)

**Investigating** - We're aware of expired images for RHEL-8.6.0 and RHEL-9.0.0. They are causing failures for `installability` test and all functional tests. We are working to resolve the problem ASAP and will try to restart all affected jobs once the issue is resolved.
