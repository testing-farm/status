---
title: Red Hat Ranch is disabled due to a wide Red Hat infrastructure outage
date: 2022-10-10 08:00:00
resolved: true
resolvedWhen: 2022-10-11 14:20:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

*Outage* - Due to a wide outage in Red Hat internal infrastructure the Red Hat Ranch is disabled. Currently there is no ETA for resolving the problem.
