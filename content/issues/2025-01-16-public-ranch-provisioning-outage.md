---
title: Public Ranch Outage - bug in security groups cleanup.
date: 2025-01-16 15:20:00
resolved: true
resolvedWhen: 2025-01-16 16:10:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---
**Resolved** 2025-01-16 16:10 UTC - The outage s resolved. Please restart your test request if they were affected.

**Outage** 2025-01-16 15:20 UTC - Public Ranch provisioning is disrupted due to hitting the security groups limits. ETA is 30 minutes.
