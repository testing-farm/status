---
title: Cyborg Development Cluster credentials outage
date: 2022-06-02 08:00:00
resolved: true
resolvedWhen: 2022-06-02 15:00:00
severity: disrupted
affected:
  - Cyborg Development Cluster
section: issue
---

**Resolved** - Problem was resolved by reinstalling the cluster.

**Outage** - The Cyborg Development Cluster credentials are invalid. We are looking into the problem. As a workaround follow https://issues.redhat.com/browse/TFT-1338.
