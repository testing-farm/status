---
title: Beaker Outage
date: 2025-02-11 14:00:00
resolved: true
resolvedWhen: 2025-02-11 18:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Outage** 2025-02-11 14:00 UTC - All workloads run on Beaker are affected. The exact time the outage begin is not yet known.

**Resolved** 2025-02-11 18:00 UTC - Beaker updates/changes/restarts are finally complete. 
