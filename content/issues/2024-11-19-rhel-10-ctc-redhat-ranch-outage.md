---
title: RHEL 10 CTC outage on Red Hat Ranch - broken compose
date: 2024-11-19 00:00:00
resolved: true
resolvedWhen: 2024-12-04 02:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resoled** 2024-12-04 02:00 UTC - few tooling fixes applied and compose added also as RHEL-10.0-ctc and RHEL-10.0-ctc1

**Updata** 2024-11-21 00:00 UTC - we already have new compose without this issue: RHEL-10.0-20241120.1, not yet promoted to CTC

**Outage** 2024-11-19 00:00 UTC - RHEL 10 CTC testing affected on Red Hat Ranch due to broken compose (https://issues.redhat.com/browse/RHEL-66937).
