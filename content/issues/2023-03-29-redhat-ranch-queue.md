---
title: Red Hat Ranch - larger queue
date: 2023-03-29 08:00:00
resolved: true
resolvedWhen: 2023-03-29 09:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2023-03-29 10:00 UTC - Queue is back to normal, resolving.

**Outage** 2023-03-29 08:00 UTC - There was a large queue in the morning, we scaled the deployments and it should be processed soon.
