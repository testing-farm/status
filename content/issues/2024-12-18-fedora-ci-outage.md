---
title: Fedora CI is not triggering on events due to expired Fedora message bus
date: 2024-12-18 09:00:00
resolved: true
resolvedWhen: 2024-12-18 15:00:00
severity: disrupted
affected:
  - Fedora CI
section: issue
---

**Resolved** 2024-12-18 15:00:00 UTC - The certificate was rotated.

**Outage** 2024-12-18 09:00:00 UTC - Fedora CI is not triggering on events, we are trying to resolve the outage. The certificate for message bus has expired today.
