---
title: Console logs are disabled due to a discovered bug
date: 2023-09-18 14:00:00
resolved: true
resolvedWhen: 2023-08-19 21:00:00
severity: disrupted
affected:
  - Red Hat Ranch
  - Public Ranch
section: issue
---

**Resolvede** 2023-09-19 21:00 UTC 

Issue was resolved in Artemis:
https://gitlab.com/testing-farm/artemis/-/merge_requests/1160

**Outage** 2023-09-18 8:00 UTC 

We have found a bug we need to address and had to temporarily disable the console log functionality.
ETA 1-2 days.
