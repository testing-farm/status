---
title: BaseOS CI Outage
date: 2022-08-12 18:00:00
resolved: true
resolvedWhen: 2022-08-15 13:00:00
severity: disrupted
affected:
  - BaseOS CI 
section: issue
---

**Resolved** - The issue was resolved by dropping the requirement for the bits stored in BRQ.

**Outage** - BaseOS CI is not processing any jobs currently, as it is broken due to a dependency from BRQ, where the LAB is being moved. We are working hard to get it back to processing jobs.
