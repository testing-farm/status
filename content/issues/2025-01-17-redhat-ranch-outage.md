---
title: Red Hat Ranch Outage - issue with Artemis deployment
date: 2025-01-17 09:00:00
resolved: true
resolvedWhen: 2025-01-17 11:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Outage** 2025-01-17 09:00 UTC - Requests in Red Hat Ranch are stuck due to Artemis outage.
