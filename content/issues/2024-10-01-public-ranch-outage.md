---
title: Public Ranch Outage - no archive disk space
date: 2024-10-01 10:00:00
resolved: true
resolvedWhen: 2024-10-01 11:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 11:00 UTC - Alert created for archive disk space. Purged old files.

**Outage** 10:00 UTC - Public Artemis stopped working, investigating. Until this is resolved, request will stay queued and will be processed later.
