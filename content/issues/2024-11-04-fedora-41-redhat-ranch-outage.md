---
title: Fedora 41 outage on Red Hat ranch
date: 2024-11-04 18:00:00
resolved: true
resolvedWhen: 2024-11-05 12:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 2024-11-04 13:00 UTC - The issue is resolved, enjoy!

**Update** 2024-11-04 13:00 UTC - The issue was identified, seems dnf5 broke the environment, and we are working on restoring the support. ETA the next day.

**Outage** 2024-11-02 13:00 UTC - Fedora 41 does not provision on Red Hat ranch. We are investigating the issue.
