---
title: Large queue times on Red Hat Ranch
date: 2023-08-07 00:00:00
resolved: true
resolvedWhen: 2023-08-07 08:22:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Update** 7:22 UTC - TF queue has been consumed, operating as usual.

**Investigating** 7:22 UTC - We're aware of an high number of requests affecting queue times on Red Hat Ranch of Testing Farm. Testing can be delayed several hours.
