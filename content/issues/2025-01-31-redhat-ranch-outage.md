---
title: Red Hat Ranch Outage - issue with Artemis deployment
date: 2025-01-31 18:00:00
resolved: true
resolvedWhen: 2025-02-03 02:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 2025-02-03 02:00 UTC - All running jobs timed out, please restart your testing. Thinks should be stable now. We are looking into how to prevent the situation.

**Outage** 2025-01-31 18:00 UTC - Requests in Red Hat Ranch are stuck due to Artemis outage.
