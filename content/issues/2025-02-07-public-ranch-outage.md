---
title: Public Ranch Outage - bug in security groups cleanup.
date: 2025-02-07 14:00:00
resolved: true
resolvedWhen: 2025-02-07 16:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2025-02-07 16:00 UTC - Issue was addressed, but the root cause - a bug in the provisioner is not yet fixed. We hope to fix it in next release.

**Outage** 2025-02-07 14:00 UTC - Public Ranch provisioning is disrupted due to hitting the security groups limits.
