---
title: Console log has wrong link in results viewer or not available at all.
date: 2023-09-15 08:00:00
resolved: true
resolvedWhen: 2023-09-26 18:00:00
severity: disrupted
affected:
  - Red Hat Ranch
  - Public Ranch
section: issue
---

**Resolved** 2023-09-26 18:00 UTC

Console log links are now correct with a hotfix. Resolving outage.

**Outage** 2023-09-15 8:00 UTC 

Red Hat ranch: Console log has wrong link in the results viewer for Red Hat ranch. We are looking into the problem. Until then, please remove the `work-.*` path from the URL. For example http://artifacts.osci.redhat.com/testing-farm/10de2a43-a2a1-4c4f-ab57-cf5e4416c93b/work-create-osbuild1e5bpd1y/console-94d0f6af-27a9-416c-9412-32bcf440dcce.log should be http://artifacts.osci.redhat.com/testing-farm/10de2a43-a2a1-4c4f-ab
      57-cf5e4416c93b/console-94d0f6af-27a9-416c-9412-32bcf440dcce.log, note the `work-create-osbuild1e5bpd1y` path removed in this case to get a valid link.

Public ranch: Console logs are not yet working on Public ranch, we are working to get them available also.
