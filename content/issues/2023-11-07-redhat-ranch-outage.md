---
title: BaseOS CI and Testing Farm Red Hat Ranch outage due to Beaker unavailability
date: 2023-11-07 15:00:00
resolved: true
resolvedWhen: 2023-11-07 19:00:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---

**Resolved** 19:00 UTC - Beaker is back, we resumed BaseOS CI and Testing Farm, jobs are processing.

**Outage** 15:00 UTC - Beaker is down including required repositories which break all executions on RH ranch for RHEL. We disabled the ranch completely to overcome too many failures. Will be posting updates here.
