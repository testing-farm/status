---
title: Public Ranch Outage - Artemis requests in error state
date: 2023-12-10 00:00:00
resolved: true
resolvedWhen: 2023-12-11 11:00:00
severity: disrupted
affected:
  - Public Ranch 
section: issue
---

**Resolved** 2023-12-11 10:00 UTC - The images were updated. The issue should be resolved.

**Outage** 2023-12-10 00:00 UTC - Our images in public ranch have expired. We are updating them.
