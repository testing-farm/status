---
title: Cyborg Loki is down
date: 2024-07-16 00:00:00
resolved: true
resolvedWhen: 2024-07-16 11:30:00
severity: disrupted
affected:
  - Cyborg Loki
section: issue
---

**Resolved** 9:30 UTC - Loki is back up. Unfortunately due to deployment having delete of pvc on scale down, we lost all the data. This misconfiguration was unknown and should not happen anymore.

**Outage** 0:00 UTC - Loki is down, we are investigating the issues.
