---
title: Covscan testing blocked on BaseOS CI 
date: 2022-03-25 12:00:00
resolved: true
resolvedWhen: 2022-03-28 12:00:00
severity: disrupted
affected:
  - BaseOS CI
section: issue
---

**Resolved** - covscan testing on BaseOS CI was resumed. Outage is over.

**Outage** - covscan is boggled with too many scans from Errata due to a known issue. We will be blocking testing until covscan is freed up and can peform CI testing. Currently there is no ETA.
