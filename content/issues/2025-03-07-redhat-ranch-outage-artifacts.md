---
title: Red Hat Ranch Outage - artifacts storage not accessible
date: 2025-03-07 10:00:00
resolved: true
resolvedWhen: 2025-03-07 16:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---


**Resolved** 2025-03-07 16:00 UTC - The DNS records on artifacts.osci.redhat.com were outdated for months. Todays maintanance exposed this problem by causing an outage. Requests will not resume, please restart your requests.

**Outage** 2025-03-07 10:00 UTC - Red Hat Ranch testing is disrupted due to artifacts storage (artifacts.osci.redhat.com) inacessible. This causes the requests to end up with empty or incomplete artifacts results.
