---
title: Testing Farm Red Hat ranch and BaseOS CI outage for RHEL9 and RHEL-8.1.0
date: 2023-09-25 00:00:00
resolved: true
resolvedWhen: 2023-09-25 16:30:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---

**Resolved** 16:30 UTC - All affected composes updated, the problems should be gone. We will try to auto-restart fail jobs, please feel free to restart yourself also if in hurry.

**Update** 12:30 UTC - All problems affecting building had been fixed. We are getting fresh composes through. ETA 2-3 hours.

**Outage** 0:00 UTC - Composes have expired due to a sad misunderstanding from Friday :( We are trying to resolve the issue, but it will take few hours.
