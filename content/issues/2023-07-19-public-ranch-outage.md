---
title: Public ranch testing blocked
date: 2023-07-19 08:00:00
resolved: true
resolvedWhen: 2023-07-19 11:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** | 2023-07-19 11:00:00 UTC

Problem was resolved by applying a hotfix to guest-setup playbooks.

**Outage** | 2023-07-19 08:00:00 UTC

We are trying to update the images for Rawhide, but it breaks due to dnf5 again.
Testing is completely blocked until we resolve the problem. ETA 2 hours.
