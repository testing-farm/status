---
title: artifacts.osci.redhat.com outage
date: 2024-12-05 13:00:00
resolved: true
resolvedWhen: 2024-12-05 15:30:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 2024-12-05 15:30:00 UTC - The outage was resolved. The issue was that IT falsly shut down the instance as unclaimed. The running jobs could be affected and never end (end up in running state). Please restart your testing if needed. New jobs are not affected from now.

**Outage** 2024-12-05 13:00:00 UTC - Archiving of artifacts in Red Hat ranch is affected due to outage artifacts.osci.redhat.com.
