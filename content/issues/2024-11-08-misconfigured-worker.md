---
title: Misconfigured worker in public ranch
date: 2024-11-08 08:00:00
resolved: true
resolvedWhen: 2024-11-08 15:00:00
severity: disrupted
affected:
  - Public Ranch 
section: issue
---

**Resolved** 2024-11-08 15:00 UTC - Outage was resolved, out workers lost connection to Artemis, manual cleanup of SG resolved the issue.

**Outage** 2024-11-08 08:00 UTC - One of the public workers was misconfigured, causing connection problems for a small number of test number of test requests.
