---
title: Fedora 41 outage on public ranch.
date: 2024-11-01 00:00:00
resolved: true
resolvedWhen: 2024-11-01 15:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2024-11-01 15:00 UTC - Fedora 41 images are updated and working now.

**Outage** 2024-11-01 00:00 UTC - Fedora 41 images are gone due to a rename, we are working on resolving the issue.
