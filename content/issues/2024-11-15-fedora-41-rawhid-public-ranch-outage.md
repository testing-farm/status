---
title: Fedora 41 and Rawhide outage on public ranch - 0xe2 breaking ansible
date: 2024-11-15 00:00:00
resolved: true
resolvedWhen: 2024-11-18 23:15:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2024-11-18 23:15 UTC - Fix for the issue had been deployed and Fedora-41 and Rawhide should work again.

**Update** 2024-11-15 13:00 UTC - dnf bug triggering our bug: https://github.com/rpm-software-management/dnf5/issues/1865

**Update** 2024-11-15 11:20 UTC - fix and test is ready, working on deployment

**Update** 2024-11-15 11:00 UTC - issue is in ensure_str(), inside gluetool. Fixing now and working on extending test

**Outage** 2024-11-15 00:00 UTC - Fedora 41 and rawhide affected by ansible crashed on decoding UTF-8 char 0xe2
