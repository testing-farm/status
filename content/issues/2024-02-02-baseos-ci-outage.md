---
title: BaseOS CI suspended for urgent jenkins controller upgrade.
date: 2024-02-02 08:00:00
resolved: true
resolvedWhen: 2024-02-02 09:00:00
severity: disrupted
affected:
  - BaseOS CI
section: issue
---

**Resolved** 09:00 UTC - Jenkins controller is running, all affected jobs were restarted.

**Outage** 08:00 UTC - All jobs (except for dispatchers) suspended as a preparation for jenkins controller upgrade. Expect delays for test result.
