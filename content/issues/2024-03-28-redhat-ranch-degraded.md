---
title: Red Hat Ranch Degraded
date: 2024-03-28 00:00:00
resolved: true
resolvedWhen: 2024-03-28 22:30:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 22:30 UTC - Beaker is back. Degradation should be over.

**Degraded** 00:00 UTC - Red Hat Ranch is degraded due to Beaker planned outage (migration to AWS). Jobs for ppc64le, s390x and specific HW requirements satisfied only by Beaker will fail. 
