---
title: Red Hat Ranch affected by RH infra outage
date: 2023-06-01 10:30:00
resolved: true
resolvedWhen: 2023-06-02 08:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** - Outage was resolved, all affected builds restarted.

**Outage** - Testing might fail due to internal RH infrastructure having a planned outage. ETA 1 day.
