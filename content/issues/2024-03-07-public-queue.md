---
title: Public Ranch large queue - expect delays
date: 2024-03-07 10:00:00
resolved: true
resolvedWhen: 2024-03-07 16:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved*** 15:00 UTC - Testing Farm queue is large again due to an outage. Expect delays, ETA 1 hour.

**Resolved*** 12:00 UTC - Testing Farm is deployed. Queue is processed. Normal operation expected.

**Outage** 10:00 UTC - Testing Farm public ranch has long queue due to ongoing deployment. It might take a bit of time the queue to go down. Please be patient.
