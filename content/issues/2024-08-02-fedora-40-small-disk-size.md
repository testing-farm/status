---
title: Fedora 40 small disk size
date: 2024-08-20 18:00:00
resolved: true
resolvedWhen: 2024-08-29 08:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2024-08-29 08:00:00 UTC - Resolved by updating to new, not broken F40 image

**Degradation** 2024-08-20 18:00 UTC - Due to recent btrfs changes in Fedora 40, the auto-resizing of root partition to the cloud image is not working. For details see https://bodhi.fedoraproject.org/updates/FEDORA-2024-67f6df918c and https://bodhi.fedoraproject.org/updates/FEDORA-2024-d44bd4abd9 bodhi updates. The issue should be fixed in 2024-08-22 once these fixes get into the fresh compose.
