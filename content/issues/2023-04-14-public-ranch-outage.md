---
title: Public Ranch - expired images
date: 2023-04-14 03:00:00
resolved: true
resolvedWhen: 2023-04-06 9:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2023-04-14 09:00 UTC - Images were expired, problem should be resolved.

**Outage** 2023-04-14 03:00 UTC -There is an outage in public ranch, we are looking into it.
