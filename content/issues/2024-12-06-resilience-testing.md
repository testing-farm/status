---
title: Resilience Testing outage
date: 2024-12-06 12:30:00
resolved: true
resolvedWhen: 2024-12-06 19:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 2024-12-06 19:00:00 UTC - The terting is finished.

**Outage** 2024-12-06 12:30:00 UTC - Due to Resilience Testinga lot of services, such as brew, mbs, dit-git are down. The planned resolvance time is 2024-12-06 19:00:00
