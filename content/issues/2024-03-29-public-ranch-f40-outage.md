---
title: Public Ranch Outage - Fedora 40 expired
date: 2024-03-29 00:00:00
resolved: true
resolvedWhen: 2024-03-29 17:00:00
severity: disrupted
affected:
  - Public Ranch 
section: issue
---

**Resolved** 17:00 UTC - Our F40 image has expired.

**Outage** 00:00 UTC - Our F40 image has expired.
