---
title: CentOS Stream 9 aarch64 broken in Public Ranch
date: 2023-04-14 00:00:00
resolved: true
resolvedWhen: 2023-04-17 20:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2023-04-17 20:00 UTC - Image was updated to a working one, issue is resolved \o/

**Outage** 2023-04-14 00:00 UTC - The CentOS Stream 9 aarch64 image is broken, all jobs will fail with `GPG Keys are configured as: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial` in pipeline.log. We are waiting for the image to refresh, until then all jobs will fail.
