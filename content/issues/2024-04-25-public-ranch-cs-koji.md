---
title: Public Ranch Outage - Random issues with access to koji
date: 2024-04-25 00:00:00
resolved: true
resolvedWhen: 2024-05-06 07:15:00
severity: disrupted
affected:
  - Public Ranch 
section: issue
---

**Outage** 2024-05-06 7:15 UTC - The issue looks resolved, we did not see it for some time.

**Outage** 2024-04-25 00:00 UTC - Random connection issues (timeout) for kojihub.stream.centos.org affecting CentOS-Stream images
