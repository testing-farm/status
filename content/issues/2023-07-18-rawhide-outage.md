---
title: Fedora Rawhide testing blocked on dnf5 breakage
date: 2023-07-18 08:00:00
resolved: true
resolvedWhen: 2023-07-18 14:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Update**

Rawhide is working again \o/.

**Outage**

The switch to dnf5 broke tmt. We are upgrading to tmt 1.25. We hope to resolve the problem today.
