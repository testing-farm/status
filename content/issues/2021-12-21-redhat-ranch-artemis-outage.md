---
title: Issues with provisioning in Red Hat Ranch
date: 2021-12-21 16:00:00
resolved: true
resolvedWhen: 2021-12-21 16:20:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

*Update* - [Issue](https://gitlab.com/testing-farm/artemis/-/issues/185) was reported to Artemis, we will continue investigation there.

*Update* - We resolved the problem by restarting the worker. We are preparing an issue to Artemis to follow up. Looks like in this case the worker was not able to reconnect to the rabbitmq for whatever reason. The outage is cleared, one more update to the issue will follow once we report the problem.

*Investigating* - We're aware of an issue affecting provisioning of machines on Red Hat Ranch of Testing Farm. Seems the problem is with our Artemis provisioning. We will be updating this issue once we know more. All queues are paused, no requests should be lost for now.
