---
title: Long queues in Public and Red Hat ranch
date: 2024-03-12 15:00:00
resolved: true
resolvedWhen: 2024-03-13 20:00:00
severity: disrupted
affected:
  - Red Hat Ranch
  - Public Ranch
section: issue
---

**Resolved** 20:00 UTC (next day) - The problems should be gone. If you see weird failures please restart, unfortunately we had outage with artifacts storage, so rsync might have timed out. Should be all ok now.

**Outage** 15:00 UTC - Long queues detected, due to workaround gone bad. We are scaling and hopefully the issues should be over. Please restart if you run into some issues. Testing should be otherwise stable.
