---
title: BaseOS CI suspended for urgent jenkins controller upgrade.
date: 2024-01-03 10:47:00
resolved: true
resolvedWhen: 2024-01-03 12:30:00
severity: disrupted
affected:
  - BaseOS CI
section: issue
---

**Resolved** 12:30 UTC - Jenkins controller is running, all affected jobs were restarted.

**Outage** 10:47 UTC - All jobs (except for dispatchers) suspended as a preparation for jenkins controller upgrade. Expect delays for test result.
