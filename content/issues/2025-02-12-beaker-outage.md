---
title: Beaker Outage - several issues
date: 2025-02-12 08:00:00
resolved: true
resolvedWhen: 2025-02-19 09:00:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---

**Outage** 2025-02-12 08:00 UTC - New release of restraint broke reservation

**Update**  2025-02-17 20:00 UTC - Issue with restraint solved, but beaker is not in a good shape: long queues, many machines marked as broken

**Update** 2025-02-18 16:00 UTC - Beaker is getting well, but we have some composes outdated, new ones bloced by issue with installing qa-tools

**Resolved** 2025-02-19 09:00 UTC - All expired composes updated, everything works as expected, beaker queues < 4h
