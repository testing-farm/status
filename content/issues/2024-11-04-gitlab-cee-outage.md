---
title: Gitlab CEE outage affecting jobs run on BaseOS CI and Red Hat ranch.
date: 2024-11-04 18:00:00
resolved: true
resolvedWhen: 2024-11-05 03:00:00
severity: disrupted
affected:
  - BaseOS CI
  - Red Hat Ranch
section: issue
---

**Resolved** 2024-11-05 03:00 UTC - The outage is over. See outage list for more details. 

**Outage** 2024-11-04 18:00 UTC - GitLab CEE outage affecting a lot of jobs.
