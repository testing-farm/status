---
title: Public Ranch - unexpected outage
date: 2023-04-28 04:00:00
resolved: true
resolvedWhen: 2023-04-28 10:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 2023-04-28 10:00 UTC - Composes were updated. We are resuming the queue. If you encountered some errors, please retry your testing.

**Outage** 2023-04-28 08:00 UTC - There is an outage in public ranch, we need to update composes. Queue is paused. ETA 2 hours. Failed tests will need restart afterwards.
