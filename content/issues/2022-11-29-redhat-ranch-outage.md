---
title: Red Hat Ranch outage - artifacts storage empty
date: 2022-11-29 8:00:00
resolved: true
resolvedWhen: 2022-11-29 10:30:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---
**Resolved** 10:30 UTC - The issue was addressed via https://gitlab.com/testing-farm/infrastructure/-/merge_requests/102. **Note that all affected jobs wil need a restart.**

**Update** 9:00 UTC - We have identified the problem and are working on a fix.

**Outage** 8:00 UTC - The artifacts.osci.redhat.com is empty after run finished executing. This is due to a bad change in the artifacts syncing code. WE are working on a fix.
