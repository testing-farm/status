---
title: Public Ranch Outage - Artemis requests in error state
date: 2024-03-19 00:00:00
resolved: true
resolvedWhen: 2024-03-19 11:00:00
severity: disrupted
affected:
  - Public Ranch 
section: issue
---

**Outage** 11:00 UTC - Images were updated. We are adding finally automation to get them updated regularly.

**Outage** 00:00 UTC - Our images in public ranch have expired. We are updating them.
