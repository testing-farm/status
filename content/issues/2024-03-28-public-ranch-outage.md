---
title: Public Ranch Outage - Expired images
date: 2024-03-28 00:00:00
resolved: true
resolvedWhen: 2024-03-28 09:30:00
severity: disrupted
affected:
  - Public Ranch 
section: issue
---

**Resolved** 09:30 UTC - The images were bumped, sorry for the outage. 

**Outage** 00:00 UTC - Our images in public ranch have expired. We are updating them.
