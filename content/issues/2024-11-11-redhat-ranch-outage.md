---
title: Red Hat Ranch / BaseOS CI Outage - provisionig issues during the weekend
date: 2024-11-09 00:00:00
resolved: true
resolvedWhen: 2024-11-11 10:00:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---

**Resolved** 2024-11-11 10:00 UTC - Queues are consumed, outage is over.

**Outage** 2024-11-11 09:00 UTC - We are working on resolving the issue in about an hour.

**Outage** 2024-11-10 00:00 UTC - Requests are blocked and the queue is rising. All requests will timeout out and restarting will be needed after the weekend.

**Outage** 2024-11-09 00:00 UTC - Due a bug causing the Artemis worker tasks to wait forever to extend reservations in PEK labs, our provisioner went down.
