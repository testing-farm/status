---
title: Red Hat Ranch Outage - large logs consuming all the space. Testing-Farm dispatcher is disabled
date: 2024-08-30 08:30:00
resolved: true
resolvedWhen: 2024-08-30 13:30:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Outage** 2024-08-30 13:30 UTC - issue was identified, outage is resolved, please retru your requests if you they failed unexpectedly or did not finish

**Outage** 2024-08-30 08:30 UTC - dispatching new jobs is disabled, investigating extensive logging which caused full disk
