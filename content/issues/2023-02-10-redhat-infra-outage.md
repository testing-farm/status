---
title: Red Hat internal infrastructure outage
date: 2023-02-10 09:00:00
resolved: true
resolvedWhen: 2023-02-11 18:00:00
severity: disrupted
affected:
  - BaseOS CI 
  - Red Hat Ranch

section: issue
---
**Outage** 18:00 UTC 11th Feb - All processing resumed, long queue for next few hours.

**Outage** 16:00 UTC - Requests to Red Hat ranch might fail unexpectedly due to the internal outage. The RHEL images depend on some of the infrastructure being down - namely repositories to beakerlib and such. The outage should last until Fri, 10 Feb 2023 22:00:00 UTC.

**Outage** 09:00 UTC - We paused processing of all jobs via BaseOS CI, to mitigate outage effects.
