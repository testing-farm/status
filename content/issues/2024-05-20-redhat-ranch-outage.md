---
title: Red Hat Ranch Outage - large queue, jobs getting delayed
date: 2024-05-19 06:00:00
resolved: true
resolvedWhen: 2024-05-19 20:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 2024-05-20 20:00 UTC - things should be stable, plus rpmdeplint and rpminspect should work again

**Update** 2024-05-20 11:00 UTC - we got into some weird problem with provisioner, causing workers to get stuck, seems we were able to get provisioner back into a working state. We will soon start enabling back the workers.

**Update** 2024-05-20 08:00 UTC - we are investigating the problem, no eta yet

**Outage** 2024-05-19 06:00 UTC - large queue started to appear during the weekend
