---
title: Public Ranch large queue - expect delays
date: 2024-03-05 15:00:00
resolved: true
resolvedWhen: 2024-03-05 21:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Outage** 21:00 UTC - Outage was resolved.

**Outage** 15:00 UTC - Testing Farm public ranch has long queue due to deployment. It might take a bit of time the queue to go down. Please be patient.
