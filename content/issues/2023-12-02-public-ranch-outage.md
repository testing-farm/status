---
title: Public Ranch Outage
date: 2023-12-02 00:00:00
resolved: true
resolvedWhen: 2023-12-02 03:00:00
severity: disrupted
affected:
  - Public Ranch 
section: issue
---

**Outage** 2023-12-02 03:00 UTC - Outage was resolved. Please restart your tests if your tests failed unexpectedly.

**Outage** 2023-12-02 00:00 UTC - We need to reprovision our Kubernetes cluster and we hit some issues trying to do that without outage. Public Ranch will be paused for 12 hours at least. Sorry for the inconvenience.
