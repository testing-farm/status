---
title: Red Hat Ranch affected by large queue
date: 2023-07-27 09:00:00
resolved: true
resolvedWhen: 2023-07-27 12:11:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** - Queue reduced from > 600 to 15

**Outage** - Adding additional 8 workers.

**Outage** - ETA 1hr

**Outage** - Queue is reducing

**Outage** - Added additional 8 workers.

**Outage** - Testing jobs may time out due to high load.

**Outage** - Added additional 8 workers.

**Outage** - Testing jobs have timed out due to high load.
