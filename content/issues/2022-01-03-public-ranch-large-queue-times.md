---
title: Large queue times on Public Ranch
date: 2022-01-03 17:00:00
resolved: true
resolvedWhen: 2022-01-04 20:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Update** - We now have better scallable workers on Public Ranch and they are dynamic and detected from the deployed infrastructure provider \o/

**Update** - We finally fixed all the issues with the new deployment and are scaling to cope up with the 1.6k+ jobs queued up. Things should be better in few hours!

**Investigating** - We're aware of an issue affecting queue times on Public Ranch of Testing Farm. Testing can be delayed several hours. Some old jobs might have failed. We are scaling the infrastructure to better cope with the high load.
