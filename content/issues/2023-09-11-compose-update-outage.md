---
title: Composes not updating on Red Hat ranch
date: 2023-09-11 08:00:00
resolved: true
resolvedWhen: 2023-08-25 12:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Update** 2023-09-25 12:00 UTC - Composes are being updated now.

**Update** 2023-09-18 8:00 UTC - Seems we finally have a a way forward here. We hope to fix the issue today finally.

**Outage** 2023-09-11 8:00 UTC - Composes are not updating properly due to issues with the build process. We are trying to fix the issue.
