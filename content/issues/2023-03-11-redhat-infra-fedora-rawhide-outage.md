---
title: Fedora-Rawhide broken in Red Hat Ranch
date: 2023-03-10 08:00:00
resolved: true
resolvedWhen: 2023-03-16 12:00:00
severity: disrupted
affected:
  - BaseOS CI 
  - Red Hat Ranch

section: issue
---

**Resolved** 2023-03-16 12:00 UTC - We have found the issue, Rawhide is broken on Xen instances. Filled to Fedora infrastructure: https://pagure.io/fedora-infrastructure/issue/11185. We workarounded by using nitro instances for the testing.

**Outage** 2023-03-10 08:00 UTC - Fedora Rawhide is broken in Red Hat ranch and BaseOS CI. The instance does not boot. Requests will error out. Currently the root cause is unknown and not ETA for resolving.
