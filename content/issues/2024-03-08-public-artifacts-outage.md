---
title: Public Ranch artifact storage ran out of disk space
date: 2024-03-08 8:00:00
resolved: true
resolvedWhen: 2024-03-08 13:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Outage** 8:00 UTC - Testing Farm public ranch artifact storage ran out of disk space. Some requests might end unexpectedly, feel free to restart them. We are working on cleaning up old large artifacts and increasing disk space.
