---
title: Red Hat Ranch Outage - issue with Artemis deployment
date: 2025-01-16 00:20:00
resolved: true
resolvedWhen: 2025-01-16 04:20:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 2025-01-16 04:20 UTC - Artemis deployment recovered and working properly again

**Outage** 2025-01-16 00:20 UTC - Red Hat ranch blocked by artemis deployment issue ind OpenShift
