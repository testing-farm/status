---
title: BaseOS CI Outage
date: 2022-12-06 8:00:00
resolved: true 
resolvedWhen: 2022-12-06 17:00:00
severity: disrupted
affected:
  - BaseOS CI 
section: issue
---

**Resolved** 17:00 UTC - The issue was resolved. We are restarting all affected jobs. It might take some time for all jobs to process.

**Outage** 15:00 UTC - We still do not have an ETA here, if the issue would persist, we will workaround it. Expecting to resume jobs this night.

**Outage** 12:00 UTC - We paused processing of all jobs, to mitigate outage effects and discussing how to mitigate this problem in the future.

**Outage** 08:00 UTC - BaseOS CI is currently failing some of it's test jobs due to an internal outage. More info in TFT-1770.
