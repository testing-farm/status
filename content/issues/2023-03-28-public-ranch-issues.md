---
title: Public ranch - Artemis resource ended in 'error' state
date: 2023-03-28 20:00:00
resolved: true
resolvedWhen: 2023-03-29 09:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Outage** 2023-03-29 20:00 UTC - Issues were resolved.

**Outage** 2023-03-28 20:00 UTC - We are seeing some provisioning instability, we believe we addressed the problem, please restart your jobs if needed.
