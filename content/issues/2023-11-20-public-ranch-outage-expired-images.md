---
title: Testing Farm Public Ranch outage due to expired images.
date: 2023-11-20 00:00:00
resolved: true
resolvedWhen: 2023-11-20 10:00:00
severity: disrupted
affected:
  - Public Ranch
section: issue
---

**Resolved** 10:00 UTC - Outage was resolved, images were updated.

**Outage** 00:00 UTC - Some of our public images have expired, we are working to get them updated.
