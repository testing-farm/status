---
title: Red Hat Ranch / BaseOS CI Outage - provisionig issues
date: 2024-09-03 20:00:00
resolved: true
resolvedWhen: 2024-09-04 13:30:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---

**Outage** 2024-09-04 02:00 UTC - We were not able to resolve the issues during night, your requests might error out or get canceled.

**Outage** 2024-09-03 20:00 UTC - We are experiencing provisioning issues on Red Hat ranch.
