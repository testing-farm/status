---
title: RHIVOS HW pool outage - several issues
date: 2025-02-24 08:28:04
resolved: true
resolvedWhen: 2025-02-24 10:26:10
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Outage** 2025-02-24 08:00 UTC - ride4-atc and rcar-s4-atc pools are discovered to be problematic.

**Update**  2025-02-24 11:00 UTC - rcar-s4-atc discovered to have been restarted automatically by systemd on Friday and did restart correctly. Fix is being worked on. ride4-atc pool investigation continues.

**Update**  2025-02-24 13:00 UTC - ride4-atc and rcar-s4-atc restored. ride4-atc pool also suffered catastrophic restarts. Investigation continues.
