---
title: OpenStack outage in Red Hat ranch
date: 2025-02-27 12:00:00
resolved: True
resolvedWhen: 2025-02-27 18:00:00
severity: disrupted
affected:
  - Red Hat Ranch
section: issue
---

**Resolved** 20205-02-27 18:00 UTC - Everything is back to normal.

**Outage** 2025-02-27 12:00 UTC - Red Hat ranch is disrupted due to OpenStack outage. Pool is not reachable and artifact pages are not accesible.

