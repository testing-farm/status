---
title: Beaker testing blocked on Red Hat Ranch and BaseOS CI
date: 2023-08-21 08:00:00
resolved: true
resolvedWhen: 2023-08-21 12:00:00
severity: disrupted
affected:
  - Red Hat Ranch
  - BaseOS CI
section: issue
---


**Resolved** 12:00 UTC - The issue was resolved by Beaker admins. Outage is resolved.

**Update** 9:00 UTC - Seems the issue is only with `/distribution/reservesys`, the task was disabled in Beaker :) We are waiting for admins to resolve.

**Outage** 8:00 UTC - All testing against Beaker is blocked due to missing `/distribution` tests from Beaker. Investigation is ongoing.
